import cv2
import cvlib as cv
from cvlib.object_detection import draw_bbox
import io
from fastapi import FastAPI, UploadFile, File, HTTPException
from fastapi.responses import StreamingResponse
from enum import Enum
import numpy as np
import nest_asyncio
import uvicorn
import os

app = FastAPI(title='My first deploy with FastAPI!')


# Using Enum to list available models, this option should be pre-defined.
class Model(str, Enum):
    yolov3tiny = "yolov3-tiny"
    yolov3 = "yolov3"


# GET method works for / endpoint:
@app.get("/")
def home():
    return "API working as expected. Now you can use http://localhost:8000/docs. "


# endpoint for images prediction:
# here need to place model and pictures to perform object detection
@app.post("/predict")
def prediction(model:Model,
               confidence:float,
               file:UploadFile = File(...)):

    # makes sure that input is correct image file:
    filename = file.filename
    fileExtension = filename.split(".")[-1] in ("jpg", "jpeg", "png")
    if not fileExtension:
        raise HTTPException(status_code=415, detail="Unsupported file format")

    # transforming images into cv2 images
    image_stream = io.BytesIO(file.file.read())
    image_stream.seek(0) # stream from beginning
    file_bytes = np.asarray(bytearray(image_stream.read()), dtype=np.uint8)
    image = cv2.imdecode(file_bytes, cv2.IMREAD_COLOR) #decoding np array

    # object detection model
    bbox, label, conf = cv.detect_common_objects(image,
                                                 confidence=confidence,
                                                 model=model)

    output_image = draw_bbox(image, bbox, conf)

    # saving in a folder in the server
    cv2.imwrite(f'uploaded_images/{filename}', output_image)

    # open saved image in binary mode for reading
    file_image = open(f'uploaded_images/{filename}', mode="rb")

    return StreamingResponse(file_image, media_type='image/jpeg')


# Spin up the server
nest_asyncio.apply()

# host should be different depending on env
host = "0.0.0.0" if os.getenv("DOCKER-SETUP") else "127.0.0.1"
uvicorn.run(app, host=host, port=8000)
