import tensorflow as tf
import datetime
import numpy
from tensorflow.keras import layers
from tersorflow.keras.metrics import RootMeansSquared as RMSE


def parse_datetime(s, format):
    if type(s) is not str:
        s = s.numpy().decode('utf-8')
    return datetime.datetime.strptime(s, format)


def get_dayofweek(s):
    ts = parse_datetime(s)
    return DAYS[ts.weekday()]


@tf.function
def dayofweek(ts_in):
    return tf.map_fn(
        lambda s: tf.py_function(get_dayofweek,
                                 inp = [s],
                                 Tout=tf.string),
        ts_in
    )


def scale_longitude(lon_column):
    return (lon_column + 78)/8


def scale_latitude(lat_column):
    return(lat_column - 37)/8


def transforms(inputs, numeric_cols, string_cols, nbuckets):
    feature_columns = {
        colname: tf.feature_column.numeric_column(colname)
        for colname in numeric_cols
    }

def euclidean(params):
    lon1, lat1, lon2, lat2 = params
    londiff = lon2 - lon1
    latdiff = lat2 - lat1
    return tf.sqrt(londiff * londiff + latdiff * latdiff)

