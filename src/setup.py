# file solve issue with import
# https://stackoverflow.com/questions/55227224/importerror-no-module-named-tensorflow-transform-beam

import setuptools

setuptools.setup(
              name='whatever-name',
              version='0.0.1',
              install_requires=[
                  'apache-beam==2.10.0',
                  'tensorflow-transform==0.12.0'
                  ],
              packages=setuptools.find_packages(),
              )
